# CTemplar (for iOS)

## Build Requirements
+ Xcode 11
+ iOS 13 SDK or later
+ Key-chain sharing
+ CocoaPods

## Runtime Requirements
+ iOS 13 or later

## About Project

Official iOS client for the CTemplar secure email service.

## Installation

run 'pod install' command in terminal in application source folder to install all need libraries, if 'pod  install' doesn't work then please try to run  'pod install --repo-update'

## Run
run Ctemplar.xcworkspace

## Security
CTemplar uses bcrypt.Swift and ObjectivePGP for hashing and encryption.

